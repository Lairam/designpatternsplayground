#pragma once
#include "ShapeGenerator.h"
class GrumpyShapeGenerator :
    public ShapeGenerator
{
public:
    GrumpyShapeGenerator();
    ~GrumpyShapeGenerator();

    virtual QGraphicsItem * makeSquare();
    virtual QGraphicsItem * makeCircle();
};

