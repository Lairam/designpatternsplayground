#include "FluffyShapeGenerator.h"
#include "FluffySquareItem.h"
#include "FluffyCircleItem.h"

FluffyShapeGenerator::FluffyShapeGenerator()
{
}


FluffyShapeGenerator::~FluffyShapeGenerator()
{
}

QGraphicsItem * FluffyShapeGenerator::makeSquare()
{
    return new FluffySquareItem();
}

QGraphicsItem * FluffyShapeGenerator::makeCircle()
{
    return new FluffyCircleItem();
}
