
#ifndef ShapeData_H
#define ShapeData_H

#include <qrect.h>
#include <qcolor.h>
#include <qpoint.h>
#include "MainWindow.h"
#include "Observer.h"

class QDataStream;

class ShapeData
{
public:
    ShapeData()
        : myBoundingBox(QRectF(0, 0, 100, 100))
        , mySelectedColor(Qt::gray)
        , myColor(Qt::lightGray)
    { }


    QRectF getBoundingBox() const { return myBoundingBox; }
    void setBoundingBox(const QRectF & boundingBox) {
        myBoundingBox = boundingBox;

        // Part of the Observer pattern. 
        notifyAllObservers();
        // end of: Part of the Observer pattern. 
    }

    QColor getColor() const { return myColor; }
    void setColor(const QColor & color)
    {
        myColor = color;

        // Part of the Observer pattern. 
        notifyAllObservers();
        // end of: Part of the Observer pattern. 
    }

    QColor getSelectedColor() const { return mySelectedColor; }
    void setSelectedColor(const QColor & selectedColor) {
        mySelectedColor = selectedColor;

        // Part of the Observer pattern. 
        notifyAllObservers();
        // end of: Part of the Observer pattern. 

    }

    // Part of the Observer pattern. 
    void addObserver(Observer * observer)
    {
        observers.append(observer);
    }
    void removeObserver(Observer * observer)
    {
        observers.removeOne(observer);
    }

    void notifyAllObservers()
    {
        foreach(Observer * observer, observers)
        {
            observer->update(this);
        }
    }
    // end of: Part of the Observer pattern. 

private:
    QRectF myBoundingBox;
    QColor mySelectedColor;
    QColor myColor;

    // Part of the Observer pattern. 
    QList<Observer*> observers;
    // end of: Part of the Observer pattern. 
};

QDataStream & operator<<(QDataStream &out, const ShapeData &shapeData);
QDataStream & operator>>(QDataStream &in, ShapeData &shapeData);

#endif ShapeData_H