#ifndef SHAPESCANVAS_H
#define SHAPESCANVAS_H

#include <QtWidgets/QMainWindow>
#include "ui_shapescanvas.h"
#include <qlist.h>

// Part of the Observer pattern. 
#include "Observer.h"
// end of: Part of the Observer pattern. 

class ShapeData;
class MyGraphicsView;
class MyGraphicsScene;
class ShapeGenerator;

class MainWindow : public QMainWindow, public Observer
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setStatusMessage(const QString & statusMessage);
    void setDefaultShapeData(const ShapeData & defaultShapeData);

    // Part of the Observer pattern. 
    virtual void update(ShapeData * observedData);
    // end of: Part of the Observer pattern. 

private:
    void createScene();
    void populateSceneWithItems();

private slots:
    void addItem();
    void addFaceItem();
    void slotSaveItemsStates();

private:
    ShapeGenerator * getTheRightGenerator() const;
private:
    Ui::ShapesCanvasClass ui;
    QGraphicsScene*scene;
    ShapeData * myDefaultShapeData;
    QList<ShapeGenerator*> myShapeGenerators;
};

#endif // SHAPESCANVAS_H
