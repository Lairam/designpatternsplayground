#ifndef LeakedClass_H
#define LeakedClass_H

#include <qdebug.h>

class LeakedClass
{
public:
    LeakedClass() { qDebug() << ("LeakedClass::constructur"); }
    ~LeakedClass() { qDebug() << ("LeakedClass::~destructor"); }
};

#endif // LeakedClass_H
