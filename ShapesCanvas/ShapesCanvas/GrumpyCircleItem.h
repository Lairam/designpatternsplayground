#pragma once
#include <qgraphicsitem.h>

class GrumpyCircleItem : public QGraphicsItem
{
public:
    GrumpyCircleItem();
    ~GrumpyCircleItem();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter * painter,
        const QStyleOptionGraphicsItem * option,
        QWidget * widget);
};

