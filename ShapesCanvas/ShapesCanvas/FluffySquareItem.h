#pragma once
#include <qgraphicsitem.h>

class FluffySquareItem : public QGraphicsItem
{
public:
    FluffySquareItem();
    ~FluffySquareItem();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter * painter,
        const QStyleOptionGraphicsItem * option,
        QWidget * widget);
};

