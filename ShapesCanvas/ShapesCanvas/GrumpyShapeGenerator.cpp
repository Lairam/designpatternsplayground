#include "GrumpyShapeGenerator.h"
#include "GrumpySquareItem.h"
#include "GrumpyCircleItem.h"


GrumpyShapeGenerator::GrumpyShapeGenerator()
{
}

GrumpyShapeGenerator::~GrumpyShapeGenerator()
{
}

QGraphicsItem * GrumpyShapeGenerator::makeSquare()
{
    return new GrumpySquareItem();
}

QGraphicsItem * GrumpyShapeGenerator::makeCircle()
{
    return new GrumpyCircleItem();
}
