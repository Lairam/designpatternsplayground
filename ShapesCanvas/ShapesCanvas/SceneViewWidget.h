#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class SceneViewWidget : public QGraphicsView
{
    Q_OBJECT

public:
    SceneViewWidget(QGraphicsScene * scene, QWidget * parent = 0);
    SceneViewWidget(QWidget * parent = 0);
    ~SceneViewWidget();

protected:
    virtual void paintEvent(QPaintEvent * ev);

private:
    
};

#endif // GRAPHICSVIEW_H
