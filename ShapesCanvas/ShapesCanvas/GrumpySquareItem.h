#pragma once
#include <qgraphicsitem.h>

class GrumpySquareItem : public QGraphicsItem
{
public:
    GrumpySquareItem();
    ~GrumpySquareItem();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter * painter,
        const QStyleOptionGraphicsItem * option,
        QWidget * widget);
};

