
#include "ShapeData.h"
#include "qdatastream.h"

QDataStream & operator<<(QDataStream &out, const ShapeData &shapeData)
{
    const QRectF box = shapeData.getBoundingBox();
    out << box.topLeft() << box.bottomRight() << shapeData.getColor() << shapeData.getSelectedColor();
    return out;
}

QDataStream & operator>>(QDataStream &in, ShapeData &shapeData)
{
    QPointF topLeft, bottomRight;
    QColor color;
    QColor selectedColor;
    in >> topLeft >> bottomRight >> color >> selectedColor;
    shapeData.setBoundingBox(QRectF(topLeft, bottomRight));
    shapeData.setColor(color);
    shapeData.setSelectedColor(selectedColor);
    return in;
}
