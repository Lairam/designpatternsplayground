#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>

class Scene : public QGraphicsScene
{
    Q_OBJECT

public:
    Scene(QObject *parent);
    ~Scene();
    virtual void drawBackground(QPainter * painter, const QRectF & rect);

};

#endif // SCENE_H
