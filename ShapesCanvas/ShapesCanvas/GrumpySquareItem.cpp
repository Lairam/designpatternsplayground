#include "GrumpySquareItem.h"
#include <QPainter.h>


GrumpySquareItem::GrumpySquareItem()
{
}


GrumpySquareItem::~GrumpySquareItem()
{
}

QRectF GrumpySquareItem::boundingRect() const
{
    return QRectF(0, 0, 200, 200);
}

void GrumpySquareItem::paint(QPainter * painter,
    const QStyleOptionGraphicsItem * option,
    QWidget * widget)
{
    painter->setBrush(Qt::darkCyan);
    painter->drawRect(boundingRect());
}
