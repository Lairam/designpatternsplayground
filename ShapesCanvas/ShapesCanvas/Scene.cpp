#include "Scene.h"

#include <qpainter.h>
#include <qvector.h>

Scene::Scene(QObject *parent)
    : QGraphicsScene(parent)
{
    int space = 10;
    QPixmap pixmap(space, space);
    pixmap.fill(Qt::white);
    QPainter painter(&pixmap);
    painter.drawPoint(0, 0);
    painter.drawPoint(space, space);
    setBackgroundBrush(pixmap);
}

Scene::~Scene()
{

}

void Scene::drawBackground(QPainter * painter, const QRectF & rect)
{
    QGraphicsScene::drawBackground(painter, rect);

    const int gridSize = 50;
    qreal left = int(rect.left()) - (int(rect.left()) % gridSize);
    qreal top = int(rect.top()) - (int(rect.top()) % gridSize);

    QVarLengthArray<QLineF, 100> gridLines;

    for (qreal x = left; x < rect.right(); x += gridSize)
        gridLines.append(QLineF(x, rect.top(), x, rect.bottom()));
    for (qreal y = top; y < rect.bottom(); y += gridSize)
        gridLines.append(QLineF(rect.left(), y, rect.right(), y));

    painter->save();
    painter->setPen(Qt::lightGray);
    painter->drawLines(gridLines.data(), gridLines.size());
    painter->restore();
}
